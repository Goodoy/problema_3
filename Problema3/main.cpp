/*Hacer una funcion que reciba dos string y verifique si es o no es igual, recorriendolo posicion por
 posicion viendo si son iguales y cuando sean no sean iguales devuelva false*/

#include <iostream>
#include <string.h>

bool palabraIgual(char *p1,char *p2); //Funcion que verifica si es igual o no

using namespace std;

int main()
{
    char palabra[]="holasbww",palabra2[]="holasbroww"; //Inicializacion de los strings


    if(palabraIgual(palabra,palabra2)){ //Verificador de si es o no igual y sus retornos
        cout<<"Son iguales"<<endl; // Si es true
    }else{
        cout<<"Son diferentes"<<endl; //Si es false
    }



}

bool palabraIgual(char *p1,char *p2) // Recibe los dos strings
{
    int rastreo=0,lon,lon2,aux=0; //
    bool check=false; //Retorno que dice si es igual o no.


    lon = strlen(p1); //Toma el tamaño del arreglo 1
    lon2 = strlen(p2); //Toma el tamaño del arreglo 2

    if(lon!=lon2){ //Primera verificación si son iguales en su tamaño
        check=false; //Si es diferente termina y devuelve el check en false
    }

    for(rastreo=0;rastreo<lon;rastreo++) //Si son iguales en el tamaño los verifica posicion por posicion.
    {
        if(p1[rastreo]==p2[rastreo]){ //Verifica la posicion rastreo en cada string
            aux+=1;//Cuenta cada verificacion correcta
        }
        else{
            break; //Si es diferente termina y devuelve el check en false
        }        
    }

    if(aux==lon){
        check=true; //Si la verificacion es correcta cambia el retorno a true.
    }

    return check;


}

